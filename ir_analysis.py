import argparse
import torch
import tvm
from tvm import relay
from tvm import target
from tvm.contrib.download import download_testdata

from torchvision import transforms
from PIL import Image
import numpy as np

import TVM_benchmark.models.build_model as build_model
from TVM_benchmark.models.layers import QuantizeContext
import TVM_benchmark.convert_model as convert_model


import os
import time
import math
import logging
import numpy as np

import torch.nn as nn
from pathlib import Path


from timm.utils import accuracy
from models import *
from utils import *



parser = argparse.ArgumentParser(description="I-ViT")

parser.add_argument("--model-name", default='deit_tiny_patch16_224',
                    choices=['deit_tiny_patch16_224',
                             'deit_small_patch16_224',
                             'deit_base_patch16_224'],
                    help="model fullname")
parser.add_argument("--model-path", default='/home/gihwan/etri/ir-analysis/archive/results/checkpoint.pth.tar',
                    help="saved checkpoint path in QAT (checkpoint.pth.tar)")
parser.add_argument("--params-path", default='/home/gihkim/workspace/ir-analysis/archive/params/params.npy',
                    help="saved parameters path in convert_model.py (params.npy)")


parser.add_argument("--model", default='deit_tiny',
                    choices=['deit_tiny', 'deit_small', 'deit_base',
                             'swin_tiny', 'swin_small', 'swin_base'],
                    help="model")
parser.add_argument('--data', metavar='DIR', default='/dataset/imagenet/',
                    help='path to dataset')
parser.add_argument('--data-set', default='IMNET', choices=['CIFAR', 'IMNET'],
                    type=str, help='Image Net dataset path')

parser.add_argument("--device", default="cuda", type=str, help="device")
parser.add_argument("--nb-classes", default=1000, type=int, help="number of classes")
parser.add_argument('--drop', type=float, default=0.0, metavar='PCT',
                    help='Dropout rate (default: 0.)')
parser.add_argument('--drop-path', type=float, default=0.1, metavar='PCT',
                    help='Drop path rate (default: 0.1)')

def forward_hook(m, inputs, output):
    # path = '/home/gihkim/workspace/ir-analysis/experiments/evaluate-ir/real'
    # module_hierarchy = str(type(m))
    # ind_s = module_hierarchy.index("'")
    # ind_r = module_hierarchy.rindex("'")
    # module_name = module_hierarchy[ind_s+1:ind_r]

    path = './'
    module_name = 'param'

    npy_path = os.path.join(path, module_name + '.npy')

    params = {}
    params['key'] = module_name

    if len(inputs) == 1:
        params['input'] = {'x' : inputs[0].cpu().numpy()}
    elif len(inputs) == 4:
        params['input'] = {'x_1' : inputs[0].cpu().numpy(), 's_1' : inputs[1].cpu().numpy(),
                           'x_2' : inputs[2].cpu().numpy(), 's_2' : inputs[3].cpu().numpy()}
    elif len(inputs) == 2:
        params['input'] = {'x' : inputs[0].cpu().numpy(), 's' : inputs[1].cpu().numpy()}
        print(f'[Torch] input scaling factor: {inputs[1].cpu().numpy()}')

    if len(output) != 2:
        params['output'] = {'x' : output[0].cpu().numpy()}
    else:
        params['output'] = {'x' : output[0].cpu().numpy(), 's' : output[1].cpu().numpy()}
        print(f'[Torch] output scaling factor: {output[1].cpu().numpy()}')

    np.save(npy_path, params)


def tvm_check(args):
    target = 'cuda'

    model = torch.load(args.model_path)
    pretrained_params = np.load(args.params_path, allow_pickle=True)[()]
    depth = 12
    convert_model.load_qconfig(model, depth)

    img_url = "https://github.com/dmlc/mxnet.js/blob/main/data/cat.png?raw=true"
    img_path = download_testdata(img_url, "cat.png", module="data")
    img = Image.open(img_path).resize((224, 224))

    my_preprocess = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    img = my_preprocess(img)
    input_image = np.expand_dims(img, 0)
    input_image = input_image / QuantizeContext.qconfig_dict['qconfig_embed_conv'].input_scale
    input_image = np.clip(input_image, -128, 127)
    input_image = np.round(input_image)
    input_image = input_image.astype("int8")

    # Load model
    name = args.model_name
    batch_size = 1
    shape = list(input_image.shape)
    image_shape = (3, 224, 224)
    data_layout = "NCHW"
    kernel_layout = "OIHW"
    func, params = build_model.get_workload(name=name,
                                            batch_size=batch_size,
                                            image_shape=image_shape,
                                            dtype="int8",
                                            data_layout=data_layout,
                                            kernel_layout=kernel_layout)
    # Build model
    pretrained_params = {**pretrained_params}
    with tvm.transform.PassContext(opt_level=3):
        lib = relay.build(func, target=target, params=pretrained_params)

    runtime = tvm.contrib.graph_executor.GraphModule(lib["default"](tvm.device(target, 0)))

    # Run model
    input_data = np.repeat(input_image, batch_size, axis=0)
    runtime.set_input('data', input_data)
    runtime.run()

    tvm_result = runtime.get_output(0).numpy()

    tvm_top1_labels = np.argsort(tvm_result[0])[::-1][:5]
    print("TVM top1 labels:", tvm_top1_labels)

    return tvm_result, input_image



def str2model(name):
    d = {'deit_tiny': deit_tiny_patch16_224,
         'deit_small': deit_small_patch16_224,
         'deit_base': deit_base_patch16_224,
         'swin_tiny': swin_tiny_patch4_window7_224,
         'swin_small': swin_small_patch4_window7_224,
         'swin_base': swin_base_patch4_window7_224,
         }
    print('Model: %s' % d[name].__name__)
    return d[name]

def analysis():
    '''
        This funckion evaluate .pth.tar file  wheter inference is well.
    '''
    args = parser.parse_args()
    device = torch.device(args.device)

    # Model
    model = str2model('deit_tiny')(pretrained=False,
                                  num_classes=args.nb_classes,
                                  drop_rate=args.drop,
                                  drop_path_rate=args.drop_path)
    checkpoint = torch.load(args.model_path, map_location=device)

    # 'blocks.{}.mlp.qact1.act_scaling_factor'].dim() is scalar so we have to change scalar to tensor.
    for key in checkpoint:
        if checkpoint[key].dim() == 0:
            checkpoint[key] = checkpoint[key].view(1)

    model.load_state_dict(checkpoint)
    model.to(device)

    # pytorch
    torch_check(model=model, device=device)
    params = np.load('./param.npy', allow_pickle=True)[()]
    params = {**params}

    # tvm
    tvm_output, tvm_input_data = tvm_check(args)

    print(f'tvm_output shape   : {tvm_output.shape}')
    print(f'torch_output shape : {params["output"]["x"].shape}')

    # de-qauntziation
    # Output of pytorch fake quantziation : q_int * scaling_factor
    output = params["output"]["x"] / params["output"]["s"]

    print('torch_output')
    print(output.astype(int))
    print()
    print('tvm_output')
    print(tvm_output)
    print()

    def l2_distance(a, b):
        return np.sqrt(np.sum((a - b)**2))

    def abs_(a, b):
        return np.abs(a-b)

    l2 = l2_distance(output, tvm_output)
    abs = abs_(output, tvm_output)
    threshold = [10, 100, 1000, 10000, 20000]

    print(f'L2 Distance={l2}')

    print("==Diff==")
    print(f'min diff={abs.min()}')
    print(f'max diff={abs.max()}')
    for thres in threshold:
        result = abs > thres
        print(f'dtype=int16,  threshold={thres},  errors={np.count_nonzero(result)}')


def torch_check(model, device):
    from tvm.contrib.download import download_testdata
    from PIL import Image

    img_url = "https://github.com/dmlc/mxnet.js/blob/main/data/cat.png?raw=true"
    img_path = download_testdata(img_url, "cat.png", module="data")
    img = Image.open(img_path).resize((224, 224))
    # Preprocess the image and convert to tensor
    my_preprocess = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    img = my_preprocess(img)
    input_image = np.expand_dims(img, 0)
    data = torch.tensor(input_image).to(device)


    # switch to evaluate mode
    model.eval()

    # ['pos_embed + patch_embed' layer]
    # model.qact1.register_forward_hook(forward_hook)
    '''
        [Torch] output scaling factor: 0.000421531789470464
        [TVM]   output scaling factor: 0.0.0004216656961943954
        (1, 197, 192)
        (1, 197, 192)
        L2 Distance=5512.25927734375
        ==Diff==
        min diff=0.0
        max diff=132.0
        dtype=int16,  threshold=100,  errors=74
        dtype=int16,  threshold=1000,  errors=0
        dtype=int16,  threshold=10000,  errors=0
    '''

    # model.blocks[0].qact1.register_forward_hook(forward_hook)
    '''
        [Torch] output scaling factor: 0.0823240801692009
        [TVM] output scaling factor: 0.08232387155294418
        tvm_output shape   : (1, 197, 192)
        torch_output shape : (1, 197, 192)

        L2 Distance=87.51571655273438
        ==Diff==
        min diff=0.0
        max diff=3.0000009536743164
        dtype=int16,  threshold=100,  errors=0
        dtype=int16,  threshold=1000,  errors=0
        dtype=int16,  threshold=10000,  errors=0
    '''

    model.blocks[0].attn.int_softmax.register_forward_hook(forward_hook)
    # model.blocks[0].attn.matmul_2.register_forward_hook(forward_hook)
    # model.blocks[0].attn.qact2.register_forward_hook(forward_hook)

    # [blocks output]
    '''
        norm 의 input 으로 측정하였음
        [Torch] input scaling factor: 0.0007026978419162333
        [TVM] output scaling factor: 0.0007409258396364748
        L2 Distance=532543.875
        ==Diff==
        min diff=0.001953125
        max diff=15070.9892578125
        dtype=int16,  threshold=100,  errors=36676
        dtype=int16,  threshold=1000,  errors=26442
        dtype=int16,  threshold=10000,  errors=87
    '''

    with torch.no_grad():
        output = model(data)

    output = output.cpu().numpy()
    top1_labels = np.argsort(output[0])[::-1][:5]
    print("top1 labels:", top1_labels)


if __name__ == "__main__":
    analysis()
