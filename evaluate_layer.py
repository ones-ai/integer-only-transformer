import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

import tvm
from tvm import relay

import argparse
parser = argparse.ArgumentParser(description="I-ViT")

# i-vit relay
from TVM_benchmark.models.layers import dequantize as Dequantize_ivit
from TVM_benchmark.models.layers import quantize as RelayQuantizeRelay_ivit
from TVM_benchmark.models.layers import quantized_gelu as RelayGELU_ivit
from TVM_benchmark.models.layers import quantized_softmax as RelaySoftmax_ivit
from TVM_benchmark.models.layers import quantized_layernorm as RelayLayerNorm_ivit
from TVM_benchmark.models.utils import QuantizeInitializer
from TVM_benchmark.models.utils import create_workload

# i-vit
from models.quantization_utils import QuantAct as QuantAct_ivit
from models.quantization_utils import IntGELU as IntGELU_ivit
from models.quantization_utils import IntSoftmax as IntSoftmax_ivit
from models.quantization_utils import IntLayerNorm as IntLayerNorm_ivit

# i-bert
from ibert.ibert_layers import QuantAct as QuantAct_ibert
from ibert.ibert_layers import IntGELU as IntGELU_ibert
from ibert.ibert_layers import IntSoftmax as IntSoftmax_ibert
from ibert.ibert_layers import IntLayerNorm as IntLayerNorm_ibert


# Timer
import timeit
parser.add_argument("--seed", default=0, type=int, help="seed")
parser.add_argument("--type", default='all', choices=['all', 'softmax', 'gelu', 'lnorm'])
parser.add_argument("--matrix-type", default='uniform', choices=['rand', 'randn', 'randint', 'uiform'])
parser.add_argument("--matrix", default=100, type=int)
                    # choices=[10, 100, 1000, 10000]) # 100, 1000, 10K
parser.add_argument("--dtype", default='int8', choices=['int8'])
parser.add_argument("--repeat", default=100, type=int, choices=[10, 100, 1000])
parser.add_argument("--target", default='cuda -model=2080ti', choices=['cuda -model=nvidia/geforce-rtx-3080', 'cuda -model=2080ti'])


# cast32 -> int8
# np.random.rand() : [0, 1), uniform disturibution
# np.random.randint() : [low, high)
# np.random.uniform() : Samples are uniformly distributed over the half-open interval [low, high)
random_collection = {   "rand" : np.random.rand,
                        "randn" : np.random.randn,
                        "randint" : np.random.randint,
                        "uniform" : np.random.uniform }

output_bit_collection = {"int8" : 8, "int32" : 32, "float32" : 32}
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class EvaluateLayer(nn.Module):
    '''
    This class is for evaluating quantized layers of I-ViT.
        Subject group : Shifmax, ShiftGELU, I-LayerNorm

        Comparsioin group :
            Original layer Pytorch FP32
            I-ViT Pytorch Fake quantization
            I-ViT Relay
            I-BERT (Pytorcch), Polly
                The following non-linear operations (GELU, Softmax, and LayerNorm) are then
                calculated on the INT32 accumulated result and then requantized back to INT8.

        Metric : matrix (100, 1000, 10K)
            Shift latency : Latency
            Accuracy : L^2 Distance
    '''
    def __init__(self, args) -> None:
        print(args)
        super(EvaluateLayer, self).__init__()
        self.args = args
        self.seed = args.seed
        self.n : int= args.matrix
        self.dtype : str = args.dtype
        self.evaluation_type : str = args.type
        self.output_bit = output_bit_collection[args.dtype]
        self.repeat = args.repeat

        torch.manual_seed(self.seed)
        torch.cuda.manual_seed(self.seed)
        np.random.seed(self.seed)
        torch.backends.cudnn.benchmark = True

        ##### random matrix #####
        # self.matrix_np = self.random_np(args.matrix_type, 1, 1, self.n, self.n)
        self.matrix_np = self.random_np(args.matrix_type, low=-4, high=4, size=(1, 1, self.n, self.n))
        self.matrix_t = torch.tensor(self.matrix_np).to(device)

        ##### Layers #####
        # 1. QuantAct : calculate I, S (Quantized, Scaling factor)
        self.qact_ivit = QuantAct_ivit(activation_bit=self.output_bit, quant_mode='symmetric')
        self.qact_ibert = QuantAct_ibert(activation_bit=output_bit_collection['int32'], quant_mode='symmetric')

        # 2. I-ViT Pytorch Fake
        self.gelu_ivit = IntGELU_ivit(output_bit=self.output_bit)
        self.softmax_ivit = IntSoftmax_ivit(output_bit=self.output_bit)
        self.layernorm_ivit = IntLayerNorm_ivit(normalized_shape=self.matrix_t.shape)

        # 3. I-ViT Relay
        self.target_acc = tvm.target.Target(args.target)
        self.data_shape_relay_ivit = (1, 1, self.n, self.n)
        self.data_layout_relay_ivit = 'NCHW'
        self.data_relay = relay.var('data', shape=self.data_shape_relay_ivit, dtype='float32') # int8

        # 4. I-BERT
        self.gelu_ibert = IntGELU_ibert(quant_mode='symmetric')
        self.softmax_ibert = IntSoftmax_ibert(output_bit=output_bit_collection['int32'], quant_mode='symmetric')
        self.layernorm_ibert = IntLayerNorm_ibert(output_bit=output_bit_collection['int32'], quant_mode='symmetric')


    @staticmethod
    def random_np(random_type, **args):
        return random_collection[random_type](**args)

    @staticmethod
    def l2_distance(baseline, x):
        return torch.sqrt(torch.sum((baseline - x)**2))

    def evaluate_accuracy(self):
        if self.evaluation_type == 'all':
            self.evaluate_gelu()
            self.evaluate_softmax()
            self.evaluate_layernorm()
        elif self.evaluation_type == 'softmax':
            self.evaluate_softmax()
        elif self.evaluation_type == 'gelu':
            self.evaluate_gelu()
        elif self.evaluation_type == 'lnorm':
            self.evaluate_layernorm()
        else:
            print('error')

    def benchmark(self, times, type):
        mean_value = np.mean(times)

        # Calculate median
        median_value = np.median(times)

        # Calculate standard deviation
        std_dev_value = np.std(times)

        # Calculate min and max
        min_value = np.min(times)
        max_value = np.max(times)
        output_string = 'Execution time summary:\n'
        output_string += " mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)  \n"
        output_string += f"   {mean_value:.4f}       {median_value:.4f}       {max_value:.4f}       {min_value:.4f}       {std_dev_value:.4f}"
        return (output_string, mean_value)



    def evaluator(self, quant_act_layer, layer, x, baseline, repeat):
        quant_x, act_scaling_factor = quant_act_layer(x)
        times = []

        for i in range(repeat):
            start, end = torch.cuda.Event(enable_timing=True), torch.cuda.Event(enable_timing=True)
            start.record()
            output, output_scaling_factor = layer(quant_x, act_scaling_factor)
            end.record()
            torch.cuda.synchronize() # Waits for all kernels in all streams on a CUDA device to complete.
            times.append(start.elapsed_time(end))


        return EvaluateLayer.l2_distance(baseline, output), act_scaling_factor, output_scaling_factor, self.benchmark(times, layer.__class__.__name__)

    def ir_evaluator(self,
                     quant_layer,
                     layer,
                     act_scaling_factor,
                     torch_scaling_factor,
                     x,
                     baseline,
                     bias_integer=None):
        #   1. layer setting
        data = quant_layer(data=self.data_relay, output_scale=act_scaling_factor.item(), out_dtype=self.dtype) # float32 -> int8

        if layer == RelayLayerNorm_ivit:
            # if layer is RelayLayerNorm, parameter is bias_int
            bias_int = bias_integer.cpu().numpy().astype(np.int32)
            B, C, H, W = bias_int.shape
            norm_bias = relay.var('norm_bias', shape=[B, C, H, W], dtype='int32')
            net = layer(data, norm_bias)
            func, _ = create_workload(relay.Function([self.data_relay, norm_bias], net) ,QuantizeInitializer())

            #   2. Relay IR build and Run
            lib = relay.build(func, target=self.target_acc, params={'norm_bias' : bias_int})
        else:
            net = layer(data, input_scale=act_scaling_factor.item())
            func, _ = create_workload(relay.Function([self.data_relay], net) ,QuantizeInitializer())

            #   2. Relay IR build
            lib = relay.build(func, target=self.target_acc, params=None)

        # 3. Run
        dev = tvm.device(str(self.target_acc), 0)
        runtime = tvm.contrib.graph_executor.GraphModule(lib["default"](dev))
        runtime.set_input('data', x)
        benchmark = runtime.benchmark(dev, repeat=self.repeat, min_repeat_ms=10)
        # runtime.run() # without execution time check

        ivit_torch = torch.tensor(runtime.get_output(0).numpy()).to(device)
        return EvaluateLayer.l2_distance(baseline, ivit_torch * torch_scaling_factor), (benchmark, benchmark.mean) # dequantization

    def printer(self, l2_ivit, l2_relay_ivit, l2_ibert, ivit_time, relay_ivit_time, ibert_time):
        l2_list = [ {'I-ViT Fake' : l2_ivit}, {'I-ViT Relay' : l2_relay_ivit}, {'I-BERT' : l2_ibert}]
        mean_time_list = [{'I-ViT Fake' : ivit_time[1]}, {'I-ViT Relay' : relay_ivit_time[1]}, {'I-BERT' : ibert_time[1]}]

        sorted_l2_list = sorted(l2_list, key=lambda x: list(x.values())[0])
        sorted_mean_time_list = sorted(mean_time_list, key=lambda x: list(x.values())[0])

        print(relay_ivit_time[0])
        print(f'\tL2 Relay I-ViT = {l2_relay_ivit}\n')
        print(ivit_time[0])
        print()
        print(f'\tL2 I-ViT = {l2_ivit}\n')
        print(ibert_time[0])
        print(f'\tL2 I-BERT = {l2_ibert}\n')

        print("정렬")
        print(f'L2      : {[list(d.keys())[0] for d in sorted_l2_list]}')
        print(f'Latency : {[list(d.keys())[0] for d in sorted_mean_time_list]}')

    def evaluate_gelu(self):
        print("=====================evaluate_gelu...=====================")
        baseline = F.gelu(self.matrix_t)

        # I-ViT Torch (Fake quantization)
        l2_ivit, act_scaling_factor, ivit_scaling_factor, ivit_time = self.evaluator(quant_act_layer=self.qact_ivit,
                                                                                        layer=self.gelu_ivit,
                                                                                        x=self.matrix_t,
                                                                                        baseline=baseline,
                                                                                        repeat=self.repeat)


        # I-ViT relay
        l2_relay_ivit, relay_ivit_time = self.ir_evaluator(quant_layer=RelayQuantizeRelay_ivit,
                                          layer=RelayGELU_ivit,
                                          act_scaling_factor=act_scaling_factor,
                                          torch_scaling_factor=ivit_scaling_factor,
                                          x=self.matrix_np,
                                          baseline=baseline)

        # I-BERT (Pytorcch), Polly
        l2_ibert, _, _, ibert_time = self.evaluator(quant_act_layer=self.qact_ibert,
                                                    layer=self.gelu_ibert,
                                                    x=self.matrix_t,
                                                    baseline=baseline,
                                                    repeat=self.repeat)

        torch.cuda.empty_cache() # GPU memory release

        self.printer(l2_ivit, l2_relay_ivit, l2_ibert, ivit_time, relay_ivit_time, ibert_time)

    def evaluate_softmax(self):
        print("=====================evaluate_softmax...=====================")
        baseline = F.softmax(self.matrix_t, dim=1)

        # I-ViT Torch (Fake quantization)
        l2_ivit, act_scaling_factor, ivit_scaling_factor, ivit_time = self.evaluator(quant_act_layer=self.qact_ivit,
                                                                                layer=self.softmax_ivit,
                                                                                x=self.matrix_t,
                                                                                baseline=baseline,
                                                                                repeat=self.repeat)

        # I-ViT relay
        l2_relay_ivit, relay_ivit_time = self.ir_evaluator(quant_layer=RelayQuantizeRelay_ivit,
                                          layer=RelaySoftmax_ivit,
                                          act_scaling_factor=act_scaling_factor,
                                          torch_scaling_factor=ivit_scaling_factor,
                                          x=self.matrix_np,
                                          baseline=baseline)

        # I-BERT (Pytorcch), Polly
        l2_ibert, _, _, ibert_time = self.evaluator(quant_act_layer=self.qact_ibert,
                                 layer=self.softmax_ibert,
                                 x=self.matrix_t,
                                 baseline=baseline,
                                 repeat=self.repeat)

        torch.cuda.empty_cache() # GPU memory release

        self.printer(l2_ivit, l2_relay_ivit, l2_ibert, ivit_time, relay_ivit_time, ibert_time)

    def evaluate_layernorm(self):
        print("=====================evaluate_layernorm...=====================")
        baseline = F.layer_norm(input=self.matrix_t, normalized_shape=self.matrix_t.shape)

        # I-ViT Torch (Fake quantization)
        l2_ivit, act_scaling_factor, ivit_scaling_factor, ivit_time = self.evaluator(quant_act_layer=self.qact_ivit,
                                                                                layer=self.layernorm_ivit,
                                                                                x=self.matrix_t,
                                                                                baseline=baseline,
                                                                                repeat=self.repeat)

        # I-ViT relay
        l2_relay_ivit, relay_ivit_time = self.ir_evaluator(quant_layer=RelayQuantizeRelay_ivit,
                                          layer=RelayLayerNorm_ivit,
                                          act_scaling_factor=act_scaling_factor,
                                          torch_scaling_factor=ivit_scaling_factor,
                                          x=self.matrix_np,
                                          baseline=baseline,
                                          bias_integer=self.layernorm_ivit.bias_integer)

        # I-BERT (Pytorcch), Polly
        l2_ibert, _, _, ibert_time = self.evaluator(quant_act_layer=self.qact_ibert,
                                 layer=self.layernorm_ivit,
                                 x=self.matrix_t,
                                 baseline=baseline,
                                 repeat=self.repeat)

        torch.cuda.empty_cache() # GPU memory release


        self.printer(l2_ivit, l2_relay_ivit, l2_ibert, ivit_time, relay_ivit_time, ibert_time)

def main():
    eval = EvaluateLayer(parser.parse_args())
    eval.to(device)
    eval.evaluate_accuracy()

if __name__=='__main__':
    main()
