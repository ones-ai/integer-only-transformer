- layer 별 output tensor 를 torch fake quantization 의 tensor 와 비교

# 실행

1. `convert_model.py` 수정
    - 패키지 의존성 때문에 `integer-only-transformer/TVM_benchmark/convert_model.py` 파일의 코드 수정
        ```python
        # 수정전
        from models.layers import QConfig, QuantizeContext

        # 수정후
        from TVM_benchmark.models.layers import QConfig, QuantizeContext

        ```
2. PyTorch I-ViT 코드에서 IntLayerNorm class 코드 수정
    - pytorch 로 작성된 코드의 경우 IntLayerNorm 의 scaling factor shape 를 제대로 설정하지 않아 수정 필요
    ```python
    # 수정전
    # self.register_buffer('norm_scaling_factor', torch.zeros(1))

    # 수정후
    self.register_buffer('norm_scaling_factor', torch.zeros(normalized_shape)) # Uncomment when load checkpoint
    ```

3. TVM code 수정
    - 확인할 tvm ir layer 의 output 부분에 module 을 리턴하면 tvm build 후 runtime 실행시 해당 tensor 가 리턴됨

    - TVM_benchmark/models/quantized_vit.py 주석 참고
    ```
    # return relay.Function(relay.analysis.free_vars(norm), norm) # Norm
    ```

4. `python ir_analysis.py --model-name deit_tiny_patch16_224 --model-path {} --params-path {}`
    ```
    pytorch 로 hook 을 생성해서 layer 의 output feature 를 가져오는 방식
    가져온 feature 를 npy 파일로 저장해 불러와서 비교
    ```

# 'pos_embed + patch_embed' layer
```
# model.qact1.register_forward_hook(forward_hook)

[Torch] output scaling factor: 0.000421531789470464
[TVM]   output scaling factor: 0.0.0004216656961943954
(1, 197, 192)
(1, 197, 192)
L2 Distance=5512.25927734375

==Diff==
min diff=0.0
max diff=132.0
dtype=int16,  threshold=100,  errors=74
dtype=int16,  threshold=1000,  errors=0
dtype=int16,  threshold=10000,  errors=0
```

# blocks output
```
# norm 의 input 측정

[Torch] input scaling factor: 0.0007026978419162333
[TVM] output scaling factor: 0.0007409258396364748
L2 Distance=532543.875

==Diff==
min diff=0.001953125
max diff=15070.9892578125
dtype=int16,  threshold=100,  errors=36676
dtype=int16,  threshold=1000,  errors=26442
dtype=int16,  threshold=10000,  errors=87
```
- block 의 입력은 문제가 없다.
- block 안에서 문제가 발생한다.



# shiftmax tvm code
```
tvm_output shape   : (1, 3, 197, 197)
torch_output shape : (1, 3, 197, 197)
torch_output
[[[[ 3029   378   182 ...   203   218   262]
   [  390  5408  1560 ...    32    39    68]
   [  457  2056  2970 ...    42    37    53]
   ...
   [  760    37    35 ...  2470  2375  1425]
   [ 1302    49    28 ...  2038  2944  2491]
   [  664   103    48 ...  1218  2325  2879]]

  [[ 6032   181   123 ...   101   181   261]
   [    3  3932   103 ...    14    51    35]
   [    1    79  1735 ...    27    54    95]
   ...
   [    1    26    66 ...  1989   478   181]
   [    0    26    48 ...   194  1839   194]
   [    4    27   104 ...    94   293  4355]]

  [[ 9022   113   103 ...   119   227   184]
   [11362  6555    42 ...     7    32    15]
   [12298    51  3311 ...    21    29    66]
   ...
   [12441    14    24 ...  7656   448   142]
   [14475    28    19 ...   243  4175   182]
   [11264    17    40 ...    84   230 11264]]]]

tvm_output
[[[[11  1  0 ...  0  0  1]
   [ 1 21  6 ...  0  0  0]
   [ 2  7 11 ...  0  0  0]
   ...
   [ 3  0  0 ... 10  8  5]
   [ 5  0  0 ...  7 11 10]
   [ 2  0  0 ...  5  8 11]]

  [[25  0  0 ...  0  0  0]
   [ 0 16  0 ...  0  0  0]
   [ 0  0  6 ...  0  0  0]
   ...
   [ 0  0  0 ...  7  1  0]
   [ 0  0  0 ...  0  7  0]
   [ 0  0  0 ...  0  1 18]]

  [[37  0  0 ...  0  0  0]
   [52 16  0 ...  0  0  0]
   [51  0 11 ...  0  0  0]
   ...
   [47  0  0 ... 29  1  0]
   [57  0  0 ...  0 17  0]
   [43  0  0 ...  0  0 43]]]]

L2 Distance=231229.1875
==Diff==
min diff=0.0
max diff=31208.0
dtype=int16,  threshold=10,  errors=106781
dtype=int16,  threshold=100,  errors=44059
dtype=int16,  threshold=1000,  errors=1809
dtype=int16,  threshold=10000,  errors=185
dtype=int16,  threshold=20000,  errors=13
```

- I-ViT softmax layer 의 activation 이 모두  0으로 출력되는 문제점을 확인
- I-ViT softmax tvm code 가 논문, pytorch 코드와 다른 부분이 있어 수정
    - 정확도는 측정이 되나 논문보다는 안좋게 나옴
    - 논문에서 shift 연산으로 softmax 를 구현할때 precision 을  right shift 횟수로 설정하는데 이때 right shift 값이  M − (k_out − 1) 으로 설정되어야한다.
    - tvm 에서는 shiftmax 의 결과가 8bit 이어 하므로 31 - (8 - 1) = 24  로 24 가 tvm 에 설정되어 있음
    - 하지만 pytorch QAT 코드에서는 K 값이 16 으로 설정되어 31 - (16 - 1) = 16 으로, 16으로 shift 값이 설정되어 있음
    - 24 로 설정할 경우 위 tvm_output 처럼 shifmax 의 output 값이 작아져 matmul -> requantization 을 수행할 경우 scaling factor 를 곱하였을 때 activiation 값이 모두 0 이되어 모든 입력값에 대해 동일한 label 을 출력하는 문제가 발생
- k (precision bit) 를 16 으로 해야 표현 값이 증가하여 activation 이 0으로 되지 않음
    - 또한 QAT 코드와 동일한 세팅이 됨


```python
def quantized_softmax(data, input_scale):
    data = relay.cast(data, 'int32')
    data_max = relay.max(data, axis=-1, keepdims=True)
    data = data - data_max

    exp_int = shift_exp(data, input_scale, 15)

    exp_int_sum = relay.sum(exp_int, axis=-1, keepdims=True)
    factor = relay.const(2**31-1, 'int32')
    # exp_int = (factor/exp_int_sum) * exp_int / relay.const(2 ** 24, 'int32')

    # 수정전
    # exp_int = relay.right_shift((factor/exp_int_sum) * exp_int, relay.const(24, dtype='int32'))
    # exp_int = relay.cast(exp_int, 'int8')

    # 수정후
    exp_int = relay.right_shift((factor/exp_int_sum) * exp_int, relay.const(16, dtype='int32'))
    exp_int = relay.cast(exp_int, 'int16')

    return exp_int
```