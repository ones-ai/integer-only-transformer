# Layer 별 비교코드

## 코드 설명
- I-BERT 논문에서 range[-4, 4] 에서 GELU 레이어를 평가하여 동일한 범위로 random matrix 를 구현하였습니다.
- latency 측정은 Relay IR 코드의 경우 TVM benchmark 함수를 사용하였습니다.
- Accuracy 측정은 pytorch fp32 layer 의 결과와 L^2 distance 를 계산하엿습니다.
  - 두논문 모두 fine-tuning 과정에서 scaing factor 를 계산한다음 적용하는 걸로 알고 있어 pytorch 코드에서 계산한 scaling factor 로 dequantization 하였습니다.
- 자세한 설명 주석 참고.

## 실행
- `python evaluate_layer.py --matrix 10 --target 'cuda -model=nvidia/geforce-rtx-3080'`
- `evaluate_layer.py` 의 argument 에 맞게 python 파일의 argument를 설정
  - `--seed`  : 난수 생성
  - `--type`  : 테스트할 레이어
    - `all`   : 전부 테스트
    - `softmax`, `gelu`, `lnorm` 
  - `--matrix-type` : matrix 생성 종류, uniform
    - `uiform`  : default, np.random.uniform
    - `rand`    : np.random.rand
    - `randn`   : np.random.randn
    - `randint` : np.random.randint
  - `--repeat` : latency 측정시, latency 튀는걸 방지하기 위해 layer 몇번 실행할지 결정
  - `--target` : TVM target device 설정 (I-ViT)
    - `cuda -model=nvidia/geforce-rtx-3080`
    - `cuda -model=2080ti`


## 결과
- `./experiments/latency/evaluate_layer/*.txt` 파일
- (1, 1, 1000, 1000) 크기 이상이 될경우 정확도는 모든 레이어에서 낮게 나왔습니다.
- 모든 레이어에서 I-ViT Relay IR 로 레이어를 구현하였을 떄 latency 가 가장 짧았습니다.
- 설정
    ```
    Namespace(seed=0, type='all', matrix_type='uniform', matrix=1000, dtype='int8', repeat=100, target='cuda -model=nvidia/geforce-rtx-3080')
    ```

### 예시
 - (1, 1, 10, 10) 행렬, [-4, 4] 범위 랜덤 행렬
    > 출력 순서는 각 Layer 모두 동일한 순서
    > 1. I-ViT Relay
    > 2. I-ViT Fake
    > 3. I-BERT

    ```
    =====================evaluate_gelu...=====================
    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.0049       0.0049       0.0051       0.0049       0.0001

            L2 Relay I-ViT = 0.5152088982253528

    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.4632       0.4597       0.6513       0.4537       0.0198

            L2 I-ViT = 0.5076305535157123

    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.1913       0.1885       0.3809       0.1864       0.0192
            L2 I-BERT = 0.08070089282442383

    =====================evaluate_softmax...=====================
    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.0081       0.0081       0.0086       0.0081       0.0001

            L2 Relay I-ViT = 9.190296396284642

    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.2898       0.2847       0.3799       0.2806       0.0176

            L2 I-ViT = 9.190638414857602

    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.6375       0.6333       0.8591       0.6267       0.0234
            L2 I-BERT = 9.000045731663704

    =====================evaluate_layernorm...=====================
    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.0112       0.0110       0.0118       0.0110       0.0003

            L2 Relay I-ViT = 1.9540013095701958

    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.4010       0.3871       1.6927       0.3810       0.1299

            L2 I-ViT = 3.677511901266311

    Execution time summary:
    mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
      0.3952       0.3922       0.4844       0.3881       0.0107
            L2 I-BERT = 9.999990600704624
    ```


# I-ViT Latency 실험

### 실행
```bash
cd ./TVM_benchmark
python evaluate_val_accuracy.py --model-name deit_tiny_patch16_224 --model-path {model_path} 
                                --params-path {params_path} --data {data_set_path} --data-set IMNET
```

### 결과

> `TVM_benhhmark/evaluate_latency.py` 를 실행시켰을때 결과
- `./latency/etri-2080ti`
  ```
   mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
   1.4183       1.4200       1.4215       1.4071       0.0036
  ```
- `./latency/my-2080ti`
  ```
  mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
    2.0396       2.0407       2.0417       2.0304       0.0031
  ```
- `./latency/my-3090ti`
  ```
  mean (ms)   median (ms)    max (ms)     min (ms)     std (ms)
    1.4472       1.4476       1.4481       1.4454       0.0009
  ```


