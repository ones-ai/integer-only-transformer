import argparse
import torch
import tvm
from tvm import relay
from tvm import target
from tvm.contrib.download import download_testdata

from torchvision import transforms
from PIL import Image
import numpy as np

import models.build_model as build_model
from models.layers import QuantizeContext

import convert_model as convert_model

import time
import logging
import os
from timm.utils import accuracy
from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
from utils.evaluate_val_accuracy_utils import dataloader, build_dataset, AverageMeter, ProgressMeter


'''
    This file is for evalauting accuracy of TVM I-ViT.
    This file evaluate all valdation dataset accuracy of ImageNet ILSVRC2012. 

    python evaluate_accuracy.py --model-name deit_tiny_patch16_224 --model-path {model_path} 
                                --params-path {params_path} --data {data_set_path} --data-set IMNET

    Namespace(batch_size=1, data='/home/gihkim/workspace/dataset/imagenet', 
                data_set='IMNET', input_size=224, model_name='deit_tiny_patch16_224', 
                model_path='{}', num_workers=8, output_dir='results/', params_path='{}', 
                pin_mem=False, print_freq=100)
'''

parser = argparse.ArgumentParser(description="TVM-Accuracy")

parser.add_argument("--model-name", default='deit_tiny_patch16_224',
                    choices=['deit_tiny_patch16_224',
                             'deit_small_patch16_224',
                             'deit_base_patch16_224'],
                    help="model fullname")
parser.add_argument("--model-path", default='',
                    help="saved checkpoint path in QAT (checkpoint.pth.tar)")
parser.add_argument("--params-path", default='',
                    help="saved parameters path in convert_model.py (params.npy)")



# for evaluation ImageNet
parser.add_argument('--data-set', default='default', choices=['CIFAR', 'IMNET', 'default'],
                    type=str, help='Image Net dataset path')
parser.add_argument('--batch-size', default=1, type=int)
parser.add_argument('--num-workers', default=8, type=int)
parser.add_argument('--pin-mem', action='store_true',
                    help='Pin CPU memory in DataLoader for more efficient (sometimes) transfer to GPU.')
parser.add_argument('--data', metavar='DIR', default='/dataset/imagenet/',
                    help='path to dataset')
parser.add_argument('--input-size', default=224, type=int, help='images input size')


parser.add_argument("--print-freq", default=100,
                    type=int, help="print frequency")
parser.add_argument('--output-dir', type=str, default='results/',
                    help='path to save log and quantized model')

def main(args):

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    # Set target device
    target = 'cuda'

    ############################# Load params ##############################
    model = torch.load(args.model_path)
    pretrained_params = np.load(args.params_path, allow_pickle=True)[()]
    depth = 12
    convert_model.load_qconfig(model, depth)

    ########################### Prepare Dataset ############################
    '''
    Set same dataloader environment with quatn_train.py
    '''
    dataset_val = build_dataset(is_train=False, args=args)
    sampler_val = torch.utils.data.SequentialSampler(dataset_val)
    loader_val = torch.utils.data.DataLoader(
            dataset_val, sampler=sampler_val,
            batch_size=args.batch_size,
            num_workers=args.num_workers,
            pin_memory=args.pin_mem,
            drop_last=False
        )

    ############################## Load Model ##############################
    print('load model ...')
    name = args.model_name
    batch_size = args.batch_size
    image_shape = (3, args.input_size, args.input_size)
    data_layout = "NCHW"
    kernel_layout = "OIHW"

    # func <class 'tvm.ir.module.IRModule'>,  params <class 'dict'>
    func, params = build_model.get_workload(name=name,
                                            batch_size=batch_size,
                                            image_shape=image_shape,
                                            dtype="int8",
                                            data_layout=data_layout,
                                            kernel_layout=kernel_layout)


    ############################## Build Model ##############################
    print('build model ...')
    pretrained_params = {**pretrained_params}

    with tvm.transform.PassContext(opt_level=3):
        lib = relay.build(func, target=target, params=pretrained_params)

    # GrapModule : Wrapper runtime module
    runtime = tvm.contrib.graph_executor.GraphModule(lib["default"](tvm.device(target, 0)))

    ############################## Debug Model ##############################
    # Uncomment this code if you use debugging mode for inference.
    # from tvm.contrib.debugger.debug_executor import GraphModuleDebug
    # runtime = GraphModuleDebug(
    #                                 lib["debug_create"]("default", tvm.device(target, 0)),
    #                                 [tvm.device(target, 0)],
    #                                 lib.graph_json,
    #                                 dump_root="/tmp/tvmdbg",)

    ############################## Logger Setting ##############################
    # logging
    logging.basicConfig(format='%(asctime)s - %(message)s',
                        datefmt='%d-%b-%y %H:%M:%S', filename=args.output_dir + 'log.log')
    logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().addHandler(logging.StreamHandler())
    logging.info(args)

    batch_time = AverageMeter('Time', ':6.3f')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(loader_val),
        [batch_time, top1, top5],
        prefix='Test: ')

    print("start evaluation...")
    end = time.time()
    for i, (data, label) in enumerate(loader_val):
        data = data.numpy()
        data = data / QuantizeContext.qconfig_dict['qconfig_embed_conv'].input_scale
        data = np.clip(data, -128, 127)
        data = np.round(data)
        data = data.astype("int8")

        # inference
        runtime.set_input('data', data)
        runtime.run()

        tvm_result = runtime.get_output(0).numpy() # [B, 1000]

        data = torch.from_numpy(data)
        label = label.unsqueeze(0)
        prec1, prec5 = accuracy(torch.from_numpy(tvm_result), label, topk=(1, 5))

        # Uncomment, if you want to print each accuracy of data.
        # np_result = runtime.get_output(0).numpy()
        # tvm_top1_labels = np.argsort(np_result[0])[::-1][:5]
        # print(f'{dataset_val.imgs[i]} prec1={prec1.data.item()} prec2={prec5.data.item()} {np.argmax(tvm_result, axis=1)} {tvm_top1_labels}' )

        top1.update(prec1.data.item(), data.size(0))
        top5.update(prec5.data.item(), data.size(0))
        
        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            progress.display(i)

    print(" * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}".format(top1=top1, top5=top5))


if __name__ == "__main__":
    args = parser.parse_args()

    main(args)
